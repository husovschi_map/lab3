package com.domain;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;

public abstract class AbstractStructuraSemestru {
    protected LocalDate startSemesterDate;

    protected LocalDate startHolidayDate;
    protected LocalDate endHolidayDate;

    protected LocalDate endSemesterDate;

    protected Calendar calendar;

    public boolean validateDate(LocalDate localDate){
        if (localDate.isAfter(this.startSemesterDate) && localDate.isBefore(this.endSemesterDate)){
            if (localDate.isBefore(this.startHolidayDate) || localDate.isAfter(this.endHolidayDate)){
                return true;
            }
            else
                return false;
        }
        return false;
    }

    public abstract int getCurrentWeek();

    public static void main(String[] argv){

        LocalDate date = LocalDate.now();
        System.out.println(date);
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd MM YYYY");
        System.out.println(dtf.format(date));
        System.out.println(date.getMonth().getValue());

    }
}
