package com.domain;

import java.time.LocalDate;

public class StructuraAnUniv {
    private static StructuraAnUniv structuraAnUnivInstance = null;
    private StructuraSemestru1 structuraSemestru1;
    private StructuraSemestru2 structuraSemestru2;

    private StructuraAnUniv(){
        this.structuraSemestru1 = StructuraSemestru1.getInstance();
        this.structuraSemestru2 = StructuraSemestru2.getInstance();
    }

    public static StructuraAnUniv getInstance() {
        if (structuraAnUnivInstance == null) {
            structuraAnUnivInstance = new StructuraAnUniv();
        }
        return structuraAnUnivInstance;
    }

    public LocalDate getCurrentDate(){
        return LocalDate.now();
    }

    public int getCurrentWeek(){
        LocalDate localDate = LocalDate.now();
        if (this.structuraSemestru1.validateDate(localDate))
            return this.structuraSemestru1.getCurrentWeek();
        if (this.structuraSemestru2.validateDate(localDate))
            return this.structuraSemestru2.getCurrentWeek();
        return 0;
    }
}
