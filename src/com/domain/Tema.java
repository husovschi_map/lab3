package com.domain;

public class Tema extends Entity <String > {
    private String descriere;
    private int startWeek;
    private int deadlineWeek;

    public Tema(String id, String descriere, int startWeek,int deadlineWeek){
        super(id);
        this.descriere = descriere;
        this.startWeek = startWeek;
        this.deadlineWeek = deadlineWeek;
    }

    public String getID() {
        return super.getId();
    }

    public void setID(String ID) {
        super.setId(ID);
    }

    public String getDescriere() {
        return descriere;
    }

    public void setDescriere(String descriere) {
        this.descriere = descriere;
    }

    public int getStartWeek() {
        return startWeek;
    }

    public void setStartWeek(int startWeek) {
        this.startWeek = startWeek;
    }

    public int getDeadlineWeek() {
        return deadlineWeek;
    }

    public void setDeadlineWeek(int deadlineWeek) {
        this.deadlineWeek = deadlineWeek;
    }

    @Override
    public String toString() {
        return "Tema{" +
                "ID='" + super.getId() + '\'' +
                ", descriere='" + descriere + '\'' +
                ", startWeek=" + startWeek +
                ", deadlineWeek=" + deadlineWeek +
                '}';
    }
}
