package com.service;

import com.domain.StructuraAnUniv;
import com.domain.Tema;
import com.repository.Repository;
import com.validator.TemaValidator;
import com.validator.ValidationException;

public class TemaService{
    private Repository<String, Tema> repository;
    private TemaValidator validator;
    private StructuraAnUniv structuraAnUniv;


    public TemaService(Repository<String, Tema> repository, TemaValidator validator, StructuraAnUniv structuraAnUniv){
        this.repository = repository;
        this.validator = validator;
        this.structuraAnUniv = structuraAnUniv;
    }

    public Tema findOne(String id) {
        return this.repository.findOne(id);
    }


    public Tema delete(String id) {
        return this.repository.delete(id);
    }

    public Iterable<Tema> findAll() {
        return this.repository.findAll();
    }

    public void save(String id, String descriere, String deadlineWeek) {//-----------startweek automate
        Tema tema = new Tema(id, descriere,this.structuraAnUniv.getCurrentWeek(), Integer.parseInt(deadlineWeek)) ;
        try {
            this.validator.validate(tema);
            this.repository.save(tema);
        }
        catch (ValidationException e) {
            e.printStackTrace();
        }
    }

    public void update(String id, String descriere, String deadlineWeek) {
        Tema oldTema = this.repository.findOne(id);
        Tema newTema = oldTema;
        if (Integer.parseInt(deadlineWeek) < this.structuraAnUniv.getCurrentWeek()){
            return;
        }
        newTema.setDescriere(descriere);
        newTema.setDeadlineWeek(Integer.parseInt(deadlineWeek)); //-------to verify if the new deadlineWeek
                                                                //-------is greater than actualCurrentWeek
        try {
            this.validator.validate(newTema);
            this.repository.update(oldTema, newTema);
        } catch (ValidationException e) {
            e.printStackTrace();
        }
    }

}
