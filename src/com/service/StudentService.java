package com.service;

import com.domain.Student;
import com.repository.Repository;
import com.validator.StudentValidator;
import com.validator.ValidationException;

public class StudentService {
    private Repository <String, Student> repository;
    private StudentValidator validator;

    public StudentService(Repository<String , Student> repository, StudentValidator validator) {
        this.repository = repository;
        this.validator = validator;
    }

    public void save(String id, String nume, String prenume, String grupa,
                     String email, String cadruDidacticIndrumatorLab) {
        Student student = new Student(id, nume, prenume, grupa, email, cadruDidacticIndrumatorLab);
        try {
            this.validator.validate(student);
            this.repository.save(student);
        }
        catch (ValidationException e) {
            e.printStackTrace();
        }
    }

    public Student findOne(String id) {
        return this.repository.findOne(id);
    }

    public Student delete(String id) {
        return this.repository.delete(id);
    }

    public Iterable<Student> findAll() {
        return this.repository.findAll();
    }

    public void update(String id, String nume, String prenume, String grupa,
                       String email, String cadruDidacticIndrumatorLab) {
        Student oldStudent = this.repository.findOne(id);
        Student newStudent = oldStudent;
        newStudent.setNume(nume);
        newStudent.setPrenume(prenume);
        newStudent.setGrupa(grupa);
        newStudent.setEmail(email);
        newStudent.setCadruDidacticIndrumatorLab(cadruDidacticIndrumatorLab);
        try {
            this.validator.validate(newStudent);
            this.repository.update(oldStudent, newStudent);
        }
        catch (ValidationException e) {
            e.printStackTrace();
        }
    }
}
