package com.validator;

import com.domain.Tema;

public class TemaValidator implements Validator<Tema> {

    @Override
    public void validate(Tema tema) throws ValidationException {
        String string = "";

        if (tema.getID().equals(""))
            string += "Incorect data! ID is invalid!\n";

        if (tema.getStartWeek() < 1 || tema.getStartWeek() > 14)
            string += "Incorect data! StartWeek is invalid!\n";

        if (tema.getDeadlineWeek() < 1 || tema.getDeadlineWeek() > 14)
            string += "Incorect data! DeadLineWeek is invalid!\n";


        if (tema.getStartWeek()  > tema.getDeadlineWeek() )
            string += "Incorect data! StartWeek is greater than DeadLineWeek!\n";

        if(string.length() > 0 )
            throw new ValidationException(string);

    }
}
