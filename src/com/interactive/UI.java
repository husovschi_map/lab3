package com.interactive;

import com.domain.Student;
import com.domain.Tema;
import com.service.StudentService;
import com.service.TemaService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class UI {

    private StudentService studentService;
    private TemaService temaService;

    public UI(StudentService studentService, TemaService temaService){
        this.studentService = studentService;
        this.temaService = temaService;
    }

    private String readString(String message){
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print(message + ":");
        try {
            String input = reader.readLine();
            while (input.length() == 0){
                System.out.println("The input cannot be blank!");
                System.out.print(message);
                input = reader.readLine();
            }

            return input;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;

    }

    public void run(){
        while (true){
            System.out.println( "0 - to exit\n" +
                                "1 - to add a Student\n" +
                                "2 - to find a Student (by ID)\n" +
                                "3 - to update a Student \n" +
                                "4 - to get all Students\n" +
                                "5 - to add a Tema\n" +
                                "6 - to find a Tema(by ID)\n" +
                                "7 - to update a Tema\n" +
                                "8 - to get all Teme\n" +
                                "9 - to delete a Student(by ID)\n" +
                                "10 - to delete a Tema (by ID)\n"
            );
            int choice = 0;
            choice = Integer.parseInt(
                    readString("Choice:")
            );

            switch (choice){

                case 0: //exit
                    return;

                case 1:{ //add a Student
                    this.studentService.save(
                            readString("ID:"),
                            readString("Nume:"),
                            readString("Prenume"),
                            readString("Grupa"),
                            readString("Email"),
                            readString("CadruDidacticIndrumatorLab:")
                    );
                    break;
                }

                case 2: { //find a Student(by ID)
                    System.out.println(
                            this.studentService.findOne(
                                    readString("ID:")
                            )
                    );
                    break;
                }

                case 3: {//update a Student
                    this.studentService.update(
                            readString("ID:"),
                            readString("Nume:"),
                            readString("Prenume"),
                            readString("Grupa"),
                            readString("Email"),
                            readString("CadruDidacticIndrumatorLab:")
                    );
                    break;
                }

                case 4:{ //get all Students
                    for(Student student:
                        this.studentService.findAll())
                                System.out.println(student);
                    break;
                }

                case 5:{ //add a Tema
                    this.temaService.save(
                            readString("ID:"),
                            readString("Descriere:"),
                            readString("DeadlineWeek:")
                    );
                    break;
                }

                case 6:{ //find a Tema by ID
                    System.out.println(
                            this.temaService.findOne(
                                    readString("ID:")
                            )
                    );
                    break;
                }

                case 7:{//update a Tema
                    this.temaService.update(
                            readString("ID:"),
                            readString("Descriere:"),
                            readString("DeadlineWeek:")
                    );
                    break;
                }

                case 8:{ //get all Teme
                    for(Tema tema:
                        this.temaService.findAll())
                                System.out.println(tema);
                    break;
                }

                case 9:{ //delete a Student
                    System.out.println("It was deleted the Student\n" +
                            this.studentService.delete(
                                    readString("ID:")
                            )
                    );
                    break;
                }

                case 10:{ //delete a Tema
                    System.out.println("It was deleted the Tema\n" +
                            this.temaService.delete(
                                    readString("ID:")
                            )
                    );
                    break;
                }

                default:
                    throw new IllegalStateException("Wrong choice mate!");
            }
        }
    }
}
