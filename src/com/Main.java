package com;

import com.domain.StructuraAnUniv;
import com.domain.StructuraSemestru1;
import com.domain.StructuraSemestru2;
import com.interactive.UI;
import com.repository.StudentRepository;
import com.repository.TemaRepository;
import com.service.StudentService;
import com.service.TemaService;
import com.validator.StudentValidator;
import com.validator.TemaValidator;

public class Main {
    public static void main(String[] args) {
        StructuraAnUniv structuraAnUniv = StructuraAnUniv.getInstance();

        StudentValidator studentValidator = new StudentValidator();
        StudentRepository studentRepository = new StudentRepository();
        StudentService studentService = new StudentService(studentRepository, studentValidator);

        TemaValidator temaValidator = new TemaValidator();
        TemaRepository temaRepository = new TemaRepository();
        TemaService temaService = new TemaService(temaRepository, temaValidator,structuraAnUniv );

        UI ui = new UI(studentService, temaService);
        ui.run();
    }
}
